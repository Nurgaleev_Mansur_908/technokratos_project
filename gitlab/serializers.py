from rest_framework import serializers

from .models import Project, Users, Merges


class ProjectGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        exclude = ("token",)


class ProjectPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ["project_id", "token"]


class UsersGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = "__all__"


class MergesGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Merges
        fields = "__all__"


class AddUsersSerializer(serializers.Serializer):
    gitlab_name = serializers.CharField()
    telegram_id = serializers.CharField()
    project_id = serializers.CharField()


class UsersSetTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ["id", "work_time"]
