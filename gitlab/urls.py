from django.urls import path
from gitlab import views
from gitlab.views import getMerge, getTelegram
from gitlab.models import Project


urlpatterns = [
    path("projects/", views.ProjectList.as_view(), name="registration"),
    path("projects/token", views.UpdateToken.as_view()),
    path("git_hook/", getMerge),
    path("tg_hook/", getTelegram),
    path("users/", views.UsersList.as_view(), name="users"),
    path("users/<str:id>/", views.UsersChange.as_view(), name="usersChange"),
]
